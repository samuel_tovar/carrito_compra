import com.garbarino.prueba.entity.Product;
import com.garbarino.prueba.repositories.ProductRepository;
import com.garbarino.prueba.service.ProductService;
import com.garbarino.prueba.service.ProductServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class ProductServiceTest {
    @Mock
    ProductRepository productRepository;
    @Test
    public void getProductById() {
        ProductService productService = new ProductServiceImpl(productRepository);
        Product product = new Product(1);
        product.setDescription("Prueba1");
        product.setUnitPrice(new BigDecimal(300.00));
        product.setStock(12);


        productService.save(product);
        Product product1 = productService.getProductById(1);

        when(productService.findById(1)).thenReturn(java.util.Optional.of(product));


        assertThat(product1).isEqualTo(product);
    }
}
