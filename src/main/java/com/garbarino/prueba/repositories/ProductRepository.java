package com.garbarino.prueba.repositories;

import com.garbarino.prueba.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Integer> {

}

