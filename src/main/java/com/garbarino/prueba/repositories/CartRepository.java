package com.garbarino.prueba.repositories;

import com.garbarino.prueba.entity.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CartRepository extends JpaRepository<Cart, Integer> {
    @Query("SELECT c FROM Cart c WHERE c.email LIKE %:email%")
    Cart findByCartByEmail(@Param("email") String email);
}
