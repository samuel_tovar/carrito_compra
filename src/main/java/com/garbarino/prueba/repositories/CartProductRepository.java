package com.garbarino.prueba.repositories;

import com.garbarino.prueba.entity.CartProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CartProductRepository extends JpaRepository<CartProduct, Integer> {
    @Query("SELECT cp FROM CartProduct cp WHERE cp.cart.id=:cartId AND cp.product.id=:productId")
    CartProduct findByCartByProduct(@Param("cartId") Integer cartId,@Param("productId") Integer productId);

}
