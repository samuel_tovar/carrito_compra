package com.garbarino.prueba.controller;

import com.garbarino.prueba.dto.CartDto;
import com.garbarino.prueba.dto.ProductDto;
import com.garbarino.prueba.entity.Cart;
import com.garbarino.prueba.exception.NotExistsProductsInStockException;
import com.garbarino.prueba.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/")
public class CartsController {

    @Autowired
    private CartService cartService;

    //[POST] /carts
    @PostMapping(path = "carts", produces = "application/json; charset=UTF-8")
    public ResponseEntity<Cart> addCart(@Valid @RequestBody Cart cart) {
        Cart c = cartService.addCart(cart);
        if(c!=null){
            return new ResponseEntity<>(c, HttpStatus.ACCEPTED);
        }else
            return new ResponseEntity<>(c,HttpStatus.NO_CONTENT);
    }

    //POST] /carts/{id}/products
    @PostMapping(path = "carts/{id}/products", produces = "application/json; charset=UTF-8")
    public ResponseEntity<Cart> addProductsCart(@Valid @RequestBody List<ProductDto> prodDto, @PathVariable("id") Integer cartId) throws NotExistsProductsInStockException {
        Cart c = cartService.addProducts(cartId, prodDto);
        if(c!=null){
            return new ResponseEntity<Cart>(c, HttpStatus.ACCEPTED);
        }else
            return new ResponseEntity<Cart>(c,HttpStatus.NO_CONTENT);
    }

    //[DELETE] /carts/{cartId}/products/{productId}
    @DeleteMapping(path = "carts/{cartId}/products/{productId}", produces = "application/json; charset=UTF-8")
    public Boolean deleteProductsCart(@PathVariable("cartId") Integer cartId,@PathVariable("productId") Integer productId) {
        return cartService.deleteCartProduct(cartId,productId);
    }
    //[GET] /carts/{id}/products
    @GetMapping(path = "carts/{id}/products", produces = "application/json; charset=UTF-8")
    public ResponseEntity<List<ProductDto>> getProductsCart(@PathVariable("id") Integer id) {
        List<ProductDto> lp = cartService.getProducts(id);
        if(lp!=null){
            return new ResponseEntity<List<ProductDto>>(lp, HttpStatus.OK);
        }else{
            return new ResponseEntity<List<ProductDto>>(lp, HttpStatus.NO_CONTENT);
        }
    }
    //[GET] /carts/{id}/
    @GetMapping(path = "carts/{id}", produces = "application/json; charset=UTF-8")
    public ResponseEntity<CartDto> getCart(@PathVariable("id") Integer id) {
        CartDto  c = cartService.getCart(id);
        if(c!=null){
            return new ResponseEntity<CartDto>(c, HttpStatus.OK);
        }else
            return new ResponseEntity<CartDto>(c,HttpStatus.NO_CONTENT);
    }

    //[POST] /carts/{id}/checkout
    @PostMapping(path = "carts/{id}/checkout", produces = "application/json; charset=UTF-8")
    public CartDto getCheckout(@PathVariable("id") Integer id) {
        return cartService.getCheckout(id,"Ready");
    }

}
