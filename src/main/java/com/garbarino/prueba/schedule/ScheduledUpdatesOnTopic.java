package com.garbarino.prueba.schedule;

import com.garbarino.prueba.service.AsyncService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;

@Component
@EnableScheduling
@EnableCaching
public class ScheduledUpdatesOnTopic {
    private static final Logger logger = LoggerFactory.getLogger(ScheduledUpdatesOnTopic.class);

    @Autowired
    private ThreadPoolTaskExecutor executor;
    @Autowired
    private AsyncService cartService;

    @Scheduled(fixedRate = 30000)
    public void processPay() throws InterruptedException, IOException {
        logger.info("processPay: started: " + new Date());
        cartService.processPay();
        //socketClient.sendMessage("smsmsmsms");
      //  executor.execute(worker);
        logger.info("processPay: finished: " + new Date());
    }
}
