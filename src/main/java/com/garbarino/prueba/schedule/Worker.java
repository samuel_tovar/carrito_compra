package com.garbarino.prueba.schedule;

import com.garbarino.prueba.service.AsyncService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class Worker implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(Worker.class);
    @Autowired
    private AsyncService cartService;
    @Override
    public void run() {
        logger.debug("worker thread started. current thread name {}", Thread.currentThread().getName());
        try {
            TimeUnit.SECONDS.sleep(3);
            cartService.processPay();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        logger.debug("worker thread finished. current thread name {}", Thread.currentThread().getName());
    }
}
