package com.garbarino.prueba.exception;

import com.garbarino.prueba.entity.CartProduct;
import com.garbarino.prueba.entity.Product;

public class NotExistsProductsInStockException extends Exception {

    private static final String DEFAULT_MESSAGE = "Not exist products in stock";

    public NotExistsProductsInStockException(CartProduct product) {
        super(DEFAULT_MESSAGE);
    }

    public NotExistsProductsInStockException(Product product) {
        super(String.format("Not exist %s products in stock. Only %d left", product.getDescription(), product.getStock()));
    }

}
