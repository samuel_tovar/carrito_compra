package com.garbarino.prueba.service;

import com.garbarino.prueba.entity.Cart;
import com.garbarino.prueba.repositories.CartRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("asyncService")
@EnableAsync
public class AsyncServiceImpl implements AsyncService {
    private static final Logger logger = LoggerFactory.getLogger(AsyncServiceImpl.class);

    @Autowired
    private CartService cartService;

    @Autowired
    private CartRepository cartRepository;

    @Async("asynchThreadPoolTaskExecutor")
    @Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public void processPay() throws InterruptedException {
        logger.info("async job started: " + new Date() + " threadId:" + Thread.currentThread().getId());
        Thread.sleep(1000);

        List<Cart> listCart = new ArrayList<>();
        cartService.findAll().forEach(
                x->{
                    if(x.getCartProducts()==null || x.getCartProducts().isEmpty()){
                        x.setStatus("FAILED");
                    }else
                    x.getCartProducts().forEach(product -> {
                                if(product.getProduct() == null || product.getProduct().getStock()<0)
                                {
                                    x.setStatus("FAILED");
                                }else{
                                    x.setStatus("PROCESSED");
                                }
                            }
                    );

                    listCart.add(x);

                }
        );

        cartService.saveAll(listCart);

        logger.info("async job finished: " + new Date() + " threadId:" + Thread.currentThread().getId());
        cartRepository.flush();
    }
}
