package com.garbarino.prueba.service;

import com.garbarino.prueba.dto.CartDto;
import com.garbarino.prueba.dto.ProductDto;
import com.garbarino.prueba.entity.Cart;
import com.garbarino.prueba.entity.CartProduct;
import com.garbarino.prueba.entity.Product;
import com.garbarino.prueba.exception.NotExistsProductsInStockException;
import com.garbarino.prueba.repositories.CartProductRepository;
import com.garbarino.prueba.repositories.CartRepository;
import com.garbarino.prueba.repositories.ProductRepository;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.*;

@Service("cartService")
public class CartServiceImpl implements CartService {

    private static final Logger logger = LoggerFactory.getLogger(CartServiceImpl.class);

    List<CartProduct> cartProducts;
    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CartProductRepository cartProductRepository;

    public List<ProductDto> getProducts(Integer id){
        try{
            Cart c = getOne(id);
            if(c!=null){
                cartProducts = c.getCartProducts();
                if(cartProducts != null) {
                    return getProductsDto();
                }
            }
        }catch (EntityNotFoundException e){
            logger.info("No existe Cart "+id+" "+e.getMessage());
            return null;
        }
        return null;
    }

    public CartDto getCart(Integer id){
        CartDto cartdto  = new CartDto();
        try{
            Cart cart = getOne(id);
            if(cart.getCartProducts()!=null) {
                cartProducts = cart.getCartProducts();
                cartdto = new DozerBeanMapper().map(cart, CartDto.class);
                cartdto.setProducts(getProductsDto());
            }
        }catch (EntityNotFoundException e){
            logger.info("No existe Cart "+id+" "+e.getMessage());
            return null;
        }
        return cartdto;
    }

    public CartDto getCheckout(Integer id,String status){
        try{
            Cart cart = getOne(id);
            if(cart!=null){
                cart.setId(id);
                cart.setStatus(status);
                cartRepository.save(cart);
            }
        }catch (EntityNotFoundException e){
            logger.info("No existe Cart "+id+" "+e.getMessage());
            return null;
        }
        return getCart(id);

    }

    public Boolean deleteCartProduct(Integer cartId, Integer productId){
        CartProduct cartProduct = cartProductRepository.findByCartByProduct(cartId,productId);
        cartProductRepository.delete(cartProduct);
        return cartProductRepository.findById(cartProduct.getId()).isPresent();
    }

    public Cart addCart(Cart cart) {
        Double total = new Double(0.0);
        Cart cp;
            cart.setTotal(BigDecimal.valueOf(0.0));
            cart.setStatus("NEW");
            cart.setCartProducts(new ArrayList<CartProduct>());
            cp = cartRepository.save(cart);

        return cp;
    }

    public Cart addProducts(Integer cartId,List<ProductDto> prodDtos) throws NotExistsProductsInStockException{
        List<Product> p = new ArrayList<>();
        Double total = new Double(0.0);
        Cart cp = null;
        try{
            cp = getOne(cartId);
        }catch (EntityNotFoundException e){
            logger.info("No existe Cart "+cartId+" "+e.getMessage());

        }
        if(cp!=null){

            List<CartProduct> cartP = new ArrayList<CartProduct>();
            Cart finalCp = cp;
            prodDtos.forEach(pro ->{
                Product product = productRepository.getOne(pro.getId());
                if(product!=null){
                    CartProduct cproduct = new CartProduct();
                    cproduct.setCart(finalCp);
                    cproduct.setProducId(pro.getId());
                    cproduct.setProduct(product);
                    cproduct.setUnitPrice(product.getUnitPrice());
                    cproduct.setQuantity(pro.getQuantity());
                    cartP.add(cproduct);
                    if(pro.getQuantity()> product.getStock()){
                        try {
                            throw new NotExistsProductsInStockException(product);
                        } catch (NotExistsProductsInStockException e) {
                            logger.info(e.getMessage(),product);
                        }
                    }else{
                        product.setStock(product.getStock()-pro.getQuantity());
                        p.add(product);
                    }
                }
            });

            cartProductRepository.saveAll(cartP);
            productRepository.saveAll(p);
            cp = cartRepository.getOne(cartId);
            cp.setTotal(getTotal(cp.getCartProducts()));
            cp.setStatus("IN_PROCESS");
            cp = cartRepository.save(cp);
            cartRepository.flush();
        }
        return cp;
    }

   // @Transactional
    public List<Cart> findAll() {
        return cartRepository.findAll();
    }

    @Transactional
    public List<Cart> saveAll(List<Cart> list) {
        return cartRepository.saveAll(list);
    }

    public Cart getOne(Integer id) {
        try{
            return cartRepository.getOne(id);
        }catch (EntityNotFoundException e){
            logger.info("No existe Cart "+id+" "+e.getMessage());
            return null;
        }

    }

    public void deleteById(Integer id) {
        cartRepository.deleteById(id);
    }

    public BigDecimal getTotal(List<CartProduct> products) {
        return products.stream()
                .map(entry -> entry.getUnitPrice().multiply(BigDecimal.valueOf(entry.getQuantity())))
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }

    public List<ProductDto> getProductsDto(){
        List<ProductDto> productDtos = new ArrayList<>();
        cartProducts.forEach(cartP -> {
                    productDtos.add(new ProductDto(cartP.getProduct().getId(),cartP.getProduct().getDescription(), cartP.getUnitPrice(),cartP.getQuantity()));
                }
        );
        return productDtos;
    }


}
