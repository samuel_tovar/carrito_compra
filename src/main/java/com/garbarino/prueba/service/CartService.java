package com.garbarino.prueba.service;

import com.garbarino.prueba.dto.CartDto;
import com.garbarino.prueba.dto.ProductDto;
import com.garbarino.prueba.entity.Cart;
import com.garbarino.prueba.entity.CartProduct;
import com.garbarino.prueba.exception.NotExistsProductsInStockException;

import java.util.List;

public interface CartService {

    List<ProductDto> getProducts(Integer id);

    CartDto getCart(Integer id);

    Cart addCart(Cart cart);

    Cart addProducts(Integer cardId, List<ProductDto> cartP) throws NotExistsProductsInStockException;

    List<Cart> findAll();

    List<Cart> saveAll(List<Cart> list);

    Cart getOne(Integer id);

    void deleteById(Integer id);

    CartDto getCheckout(Integer id,String status);

    Boolean deleteCartProduct(Integer cartId, Integer productId);

}
