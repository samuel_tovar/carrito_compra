package com.garbarino.prueba.service;

import com.garbarino.prueba.entity.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    public List<Product> findAll();

    public Optional<Product> findById(Integer id);

    public Product getProductById(Integer id);

    public Product save(Product stock);

    public void deleteById(Integer id);
}
