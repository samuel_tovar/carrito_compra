package com.garbarino.prueba.service;

import com.garbarino.prueba.entity.Product;
import com.garbarino.prueba.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("productService")
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public Optional<Product> findById(Integer id) {
        Optional<Product> p = productRepository.findById(id);
        return p;
    }

    public Product getProductById(Integer id) {
        return findById(id).orElse(null);
    }

    public Product save(Product stock) {
        return productRepository.save(stock);
    }

    public void deleteById(Integer id) {
        productRepository.deleteById(id);
    }
}
