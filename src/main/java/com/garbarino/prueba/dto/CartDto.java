package com.garbarino.prueba.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.CreationTimestamp;

import javax.validation.constraints.Past;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CartDto {
    Integer id;

    private String fullName;

    private String email;

    private BigDecimal total;

    private String status;

    @JsonProperty("creationDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Past
    private Date creationDate;

    private List<ProductDto> Products = new ArrayList<>();

    public CartDto() {
    }

    public CartDto(Integer id, String fullName, String email, BigDecimal total, String status, @Past Date creationDate, List<ProductDto> products) {
        this.id = id;
        this.fullName = fullName;
        this.email = email;
        this.total = total;
        this.status = status;
        this.creationDate = creationDate;
        Products = products;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public List<ProductDto> getProducts() {
        return Products;
    }

    public void setProducts(List<ProductDto> products) {
        Products = products;
    }
}
