package com.garbarino.prueba.dto;

import com.garbarino.prueba.entity.Product;

import java.math.BigDecimal;

public class ProductDto  {
    Integer id;
    String description;
    BigDecimal unitPrice;
    Integer quantity;

    public ProductDto(Integer id, String description, BigDecimal unitPrice, Integer quantity) {
        this.id = id;
        this.description = description;
        this.unitPrice = unitPrice;
        this.quantity = quantity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
